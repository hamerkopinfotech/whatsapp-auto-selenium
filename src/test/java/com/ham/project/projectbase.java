package com.ham.project;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URLEncoder;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;

public class projectbase {
 
	
public WebDriver driver;

  @BeforeClass
  public void beforeMethod()
  
  {
	  
       System.setProperty("webdriver.chrome.driver","C:\\whatsapp-auto-selenium\\driver\\chrome\\chromedriver.exe");
		  driver=new ChromeDriver();
		  driver.manage().window().maximize();
		  driver.manage().timeouts().implicitlyWait((long)24, TimeUnit.HOURS);
	  
  }

  @AfterClass
  public void afterMethod() 
  {
	  
	  driver.quit();
  }

  
  
  @DataProvider
  
  
  public Object[][] dp() 
  {
    
   
	  FileInputStream fis = null;
	try {
		fis = new FileInputStream("C:\\xampp\\htdocs\\whatsapp-auto\\media\\excel\\excel.xls");
	} catch (FileNotFoundException e1) {
		
		e1.printStackTrace();
	}
      Workbook wb = null;
	try {
		wb = Workbook.getWorkbook(fis);
	} catch (BiffException | IOException e) {
		
		e.printStackTrace();
	}
      Sheet sh=wb.getSheet("Sheet1");
      int rows=sh.getRows();
      int columns=sh.getColumns();
      System.out.println(rows);
      System.out.println(columns);
      
      String inputdata[] []=new String [rows] [columns];
      
     for(int i=0;i<rows;i++)
     {
    	 for(int j=0;j<columns;j++)
    	 {
    		 Cell c=sh.getCell(j,i);
    		
    		 inputdata [i][j]=c.getContents();
    	 
     }
     }
    	
    	  return inputdata;
    	 
    	 
    }
  
  


public static String encode(String url)  
{  
          try {  
               String encodeURL=URLEncoder.encode( url, "UTF-8" ).replace("+", "%20"); 
               return encodeURL;  
          } catch (Exception e) {  
               return "Issue while encoding" +e.getMessage();  
          }  
}


}


		 
    		 


    		 
    		 

