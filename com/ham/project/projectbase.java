package com.ham.project;

import org.testng.annotations.Test;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;

public class projectbase {
	
public WebDriver driver;

  @BeforeClass
  public void beforeMethod()
  
  {
	  
       System.setProperty("webdriver.chrome.driver","C:\\Users\\MY PC\\Downloads\\chromedriver_win32\\chromedriver.exe");
		  driver=new ChromeDriver();
		  //driver.get("https://web.whatsapp.com");
		  driver.manage().window().maximize();
		  driver.manage().timeouts().implicitlyWait((long) 4, TimeUnit.HOURS);
	  
  }

  @AfterClass
  public void afterMethod() 
  {
	  
	  driver.quit();
  }

  
  
  @DataProvider
  public Object[][] dp() throws BiffException, IOException 
  {
    
   
	  FileInputStream fis=new FileInputStream("C:\\Users\\MY PC\\Desktop\\auto.xls");
      Workbook wb= Workbook.getWorkbook(fis);
      Sheet sh=wb.getSheet("Sheet1");
      int rows=sh.getRows();
      int columns=sh.getColumns();
      System.out.println(rows);
      System.out.println(columns);
      
      String inputdata[] []=new String [rows] [columns];
      
     for(int i=0;i<rows;i++)
     {
    	 for(int j=0;j<columns;j++)
    	 {
    		 Cell c=sh.getCell(j,i);
    		 
    		 inputdata [i][j]=c.getContents();
    		 
    		 System.out.println(inputdata [i][j]);
    	 }
     }
      return inputdata;
      
 
    }



}


